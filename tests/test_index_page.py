from time import sleep

import allure
import requests

import config


@allure.suite("Index Page tests")
class TestIndexPage:
    """Tests for Index Page: """

    @allure.title("Index page content verification")
    def test_index_page_is_loaded(self):
        """Verify that index page content is loaded correctly"""
        # When
        response = requests.get(config.base_url)
        # Then
        assert response.status_code == 200
        assert 'Hello, World!' in response.text
